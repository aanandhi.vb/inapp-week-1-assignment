# InApp Week - 1 Assignment
# Author      : Aanandhi V B
# Date        : November 6, 2020
# Description : A program to implement the Rock-Paper-Scissors Game
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-1-assignment/-/blob/master/rock_paper_scissors_game.py

import random

print('''
ROCK PAPER SCISSORS GAME
------------------------
    How to play
    -----------     
Rock beats Scissors
Scissors beats Paper
Paper beats Rock

Press:
    0 - Rock
    1 - Paper
    2 - Scissors
''')


# Function to decide winner in each round
def rounds(computer_choice, user_choice):
    user_point = 0
    computer_point = 0
    if computer_choice == user_choice:
        # Go to next round
        print("Tie!")
        return "None", 0, 0

    elif ((computer_choice == 0) and (user_choice == 2)) or ((computer_choice == 2) and (user_choice == 1)) or (
            (computer_choice == 1) and (user_choice == 0)):
        # Computer Wins
        computer_point += 1
        print("Computer Won!")

    else:
        # User Wins
        user_point += 1
        print("User Won!")

    winner = "Computer" if (computer_point > user_point) else "User"
    return winner, user_point, computer_point


# Function to get user input for each round and declare final winner
def start_game():

    game_history = {}
    user_total, computer_total = 0, 0
    counter = 1
    while counter < 11:
            print("\n----- ROUND {} -----\n".format(counter))
            computer_choice = random.choice([0, 1, 2])
            try:
                user_choice = int(input("Enter your choice: "))
                if user_choice > 2:
                    print("Enter a choice between 0-2!")

                else:
                    winner, user_point, computer_point = rounds(computer_choice, user_choice)
                    user_total += user_point
                    computer_total += computer_point
                    game_history[str(counter)] = {"User": "Rock" if (user_choice == 0) else "Paper" if (user_choice == 1)
                                                  else "Scissors", "Computer": "Rock" if (computer_choice == 0)
                                                  else "Paper" if (computer_choice == 1)
                                                  else "Scissors", "Winner": winner}
                    counter += 1

            except ValueError:
                print("Enter valid input!")

    print("\n----- POINTS -----\n")
    print("Computer: {}".format(computer_total))
    print("User: {}".format(user_total))
    print("The winner is {}".format("Computer" if (computer_total > user_total) else "User" if (computer_total < user_total) else "None"))
    while True:
        try:
            round_info = int(input("\nEnter the round between 0-10 for which you need the information: "))
            if round_info < 11:
                    print("User Choice = {}".format(game_history[str(round_info)]["User"]))
                    print("Computer Choice = {}".format(game_history[str(round_info)]["Computer"]))
                    print("{} won round {}".format(game_history[str(round_info)]["Winner"], round_info))
                    print("\nThank you for playing!")
                    break
            else:
                    print("Invalid Round!")

        except ValueError:
            print("Enter valid input!")


start_game()
